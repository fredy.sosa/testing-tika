# Golang apache tika's POC
### What do you need for this project? 
* [Install java ](https://java.com/en/download/help/download_options.html)
* [Install Golang ](https://golang.org/dl/)
* Create a folder named files and other one named results
* Put on "files" all the files you want to parse. You can download a lot of example file formats [here](https://file-examples.com/) or also [here](https://filesamples.com/)
### Running tika standalone
#### For a single file
* `java -jar tika-app-2.1.0.jar <options> <path-to-file>`
* Run `java -jar tika-app-2.1.0.jar --help` for more information
#### For many file on a batch mode
* `java -jar tika-app-2.1.0.jar -i <input-directory> -o <output-directory> <options>`
* Run `java -jar tika-app-2.1.0.jar --help` for more information
### Running the program
* `go run main.go`
* The expected output is going to be the whole contentType of the files you put on the "files" folder and the time it took to run the whole functionality, i. e.:
![referenceImage](image.png)